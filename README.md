---
sidebar: auto
sidebarDepth: 0
---

# :book: Receitas :woman_cook:

## :cake::lemon: Bolo de limão ##

### **Ingredientes** ###

**Para o bolo**

- ¼ de um limão-taiti (sem a parte branca)
- 4 ovos grandes inteiros :egg:
- ½ xícara de óleo
- 1 colher de sopa de fermento em pó
- 3 xícaras de farinha de trigo
- 1 pitada de sal
- 4 colheres de sopa de água
- 2 xícaras rasas de açúcar

Observação: se não houver limão-taiti à disposição para o preparo, ele pode ser substituído por uma gelatina de limão inteira.

**Para a cobertura**

- 1 lata de leite condensado
- Suco de 2 limões
- Raspa de limão

### Modo de preparo ###

O preparo da cobertura requer a mistura do leite condensado com o suco de limão, até dar consistência o suficiente para ficar em reserva.

Inicialmente para o bolo, bata no liquidificador os ovos, o limão (ou a gelatina), o açúcar, o óleo, o sal e a água. Em seguida, coloque em um recipiente e acrescente farinha de trigo peneirada. Ao fim, insira o fermento em pó.

Na sequência leve o bolo ao forno pré-aquecido, em temperatura de 200°C por 30 minutos. Quando o bolo estiver assado, coloque a cobertura por cima. Por último, insira as raspas do limão para enfeitar.

----

## :cake::chocolate_bar::peanuts: Bolo de cacau com paçoca ##

### Ingredientes ###

- 5 ovos :egg:
- 1 xícara de óleo
- 1 xícara de açúcar
- 2 xícaras de farinha de trigo
- 1 xícara de leite integral
- 1 xícara de chocolate em pó 50% cacau
- 2 colheres de sobremesa de fermento em pó

### Modo de preparo ###

1. Separe as claras das gemas, e bata as claras até que fique em ponto médio.
2. Em seguida adicione as gemas uma a uma enquanto bate tudo.
3. Adicione o óleo e bata bem, depois o açúcar e deixe bater mais um pouco.
4. Aos poucos adicione a farinha e o leite, intercalando, um pouquinho de farinha, um pouquinho de leite, até que toda a medida tenha sido adicionada.
5. Depois adicione o chocolate em pó e bata mais um pouco
6. Por último o fermento e bata um pouquinho até misturar bem.
7. Em uma forma untada com manteiga e enfarinhada com o chocolate em pó, adicione a massa e leve ao forno preaquecido a 180 graus e deixe assar por
8. cerca de 30 minutos dependendo do forno. Ou quando o palitinho sair seco.
9. Deixe esfriar totalmente antes de esfarelar o bolo.

### Cobertura ###

1 lata de leite condensado

1 lata de creme de leite

1 colher de manteiga

Paçoca para polvilhar

----



## :birthday: Brownie na marmita ##

### **Ingredientes** ###

- 200g de chocolate meio amargo ou ao leite
- 4 colheres (sopa) de manteiga ( 80g)
- 4 ovos
- 1 e ½ xícara (chá) de açúcar (265g)
- 1 colher (café) de essência de baunilha
- ½ xícara (chá) de chocolate em pó (70g)
- 1 xícara (chá) de farinha de trigo ( 120g)

### **Ingredientes e sugestões de sabores** ###

- Tradicional : não acrescente nada na massa.
- Nutella (creme de avelã) a gosto
- Nozes a gosto
- M&M’s a gosto
- Bis a gosto
- Biscoito Oreo a gosto

Derreta o chocolate com a manteiga em banho-maria ou no micro-ondas de 30 em 30 segundos e mexa a cada tempo até que derreta por completo. Reserve.

Em outra tigela coloque os ovos, o açúcar e a essência de baunilha, misture bem até que fique bem homogêneo. Depois adicione o chocolate e a manteiga já derretida, misture bem. Em seguida, adicione o chocolate em pó e a farinha de trigo e misture mais um pouco. Despeje em uma assadeira untada com manteiga e polvilhada com chocolate em pó, ou em marmitas. Para as marmitas não há necessidade de untar com manteiga.

- Para o sabor tradicional não precisa acrescentar mais nada, apenas leve ao forno.
- Para o sabor de Bis : coloque sobre a massa pedacinhos de Bis a gosto.


- Para o sabor de biscoito Oreo: coloque sobre a massa pedacinhos de Oreo a gosto.


- Para o sabor m&m’s: coloque sobre a massa os m&m’s a gosto.


- Para o sabor de Nozes : coloque sobre a massa pedacinhos de Nozes picada a gosto.


- Para o sabor de Nutella : coloque pequenas porções de Nutella sobre o papel manteiga ou superfície lisa e leve ao freezer para endurecer. Só depois coloque sobre a massa.


Leve ao forno preaquecido a 180°C por aproximadamente 30 minutos.


Retire do forno e deixe esfriar.

Rendimento 6 marmitas de 150g cada.

----

## :honey_pot: Bolo pão de mel ##

### **Ingredientes** ###

- 2 ovos
- 1 lata de leite condensado
- 1 copo de leite
- 1 xícara de mel
- 1 xícara de açúcar mascavo
- 3 colheres de cacau em pó
- 1 colher de chá de cravo em pó
- 1 colher de chá de canela em pó
- Raspas de noz moscada
- 1 xícara de nozes (Opcional. E se quiser pode por menos também)
- 1 colher de chá de bicarbonato
- 1 colher de sopa de fermento em pó
- 3 xícara de farinha de trigo

### Recheio ###

- 2 latas de leite condensado cozida
- 800 gramas de doce de leite comprado pronto

### Cobertura ###

- 1 caixinha de creme de leite
- 250 gramas de chocolate 1/2 amargo picadinho

Leve o creme de leite ao micro-ondas por 1 minuto.

Coloque o chocolate e mexa bem até derreter.

PS: Se necessário, coloque mais 10 segundos no micro.

### Calda ###

- 1 copo de água
- 1 colher de sopa de açúcar
- 1 pau de canela (ou 1 colher de sopa de canela)

Leve ao micro-ondas por 3 minutos.

### **Modo de fazer** ###

1. Coloque no liquidificador todos os ingredientes, exceto a farinha de trigo, fermento e bicarbonato. Bata bem.
2. Acrescente os 3 ingredientes que ficaram de fora e bata, mas não muito. Vá dando uma ajuda amiga com uma colher.
3. Coloque em uma forma untada de 30cm de diâmetro.
4. Leve ao forno preaquecido a 180ºC por 40 minutos (ou faça o teste do palitinho, ele tem que sair sequinho)
5. Deixe amornar e desenforme.
6. Corte-o ao meio e regue com metade da calda. Eu reguei com um pincel (desses de untar a forma, sabe?), acho melhor, pois não cai um montão em um lugar só.
7. Recheie com o doce de leite e feche o bolo com a outra metade.
8. Regue com o restante da calda.
9. Cubra o bolo com a cobertura de ganache.

-----

## :honey_pot: Bolo pão de mel - Edu Guedes ##

### **Ingredientes** ###

**Rendimento:** 10 pessoas

**Tempo:** 1 hora :clock1:

**Bolo**

- 1 xícara (chá) de mel
- Suco de 1 limão grande
- 1 xícara (chá) de açúcar mascavo
- 1 xícara (chá) de leite
- 1 colher (sopa) de manteiga
- ½ colher (chá) de canela em pó
- ½ colher (café) de cravo moído
- 3 xícaras (chá) de farinha de trigo
- 1 colher (sobremesa) de bicarbonato de sódio

**Recheio e cobertura**

- 1 xícara (chá) de doce de leite molinho
- 1 xícara (chá) de guaraná para molhar o bolo
- 2 litros de sorvete
- ½ Kg de chocolate fracionado

**Brigadeiro falso**

- 1 xícara (chá) de farelo de pão de mel
- 1 colher (sopa) de leite condensado
- 2 colheres (sopa) chocolate em pó

### **Modo de Preparo** ###

**Bolo**

Em uma tigela, coloque o mel e o suco de limão e misture bem. Acrescente o açúcar, o leite, a manteiga e bata até que os ingredientes fiquem homogêneos. Coloque a canela, o cravo e a farinha e deixe a massa homogênea Adicione o bicarbonato e deixe incorporar. Asse em forma untada e enfarinhada por 25 a 30 minutos em forno médio. Quando estiver pronto, retire do forno e deixe esfriar.

**Montagem**

Desenforme, corte uma tampa, retire o miolo, molhe o bolo com guaraná e recheie com doce de leite e sorvete. Coloque a tampa por cima e leve ao freezer. Cubra com chocolate derretido.

**Brigadeiro falso**

Use o miolo do pão de mel, esfarele e misture o leite condensado. Faça bolinhas e passe no chocolate em pó.

----

## :chicken: Torta de frango de requeijão de liquidificador ##

### Ingredientes ###

- 1 peito de frango
- 1 lata de milho
- 1 copo de requeijão
- 2 tomates
- 1/2 cebola
- 1 caldo de galinha
- 200 g de mussarela
- 2 xícaras de farinha de trigo
- 2 xícaras de leite desnatado
- 1/2 xícara de óleo
- 3 ovos
- 1 saquinho de queijo parmesão ralado
- alho poró
- sal e pimenta a gosto
- 1 colher (sopa) de fermento
- manteiga para untar

### Modo de preparo ###

**Massa**

1. Bata no liquidificador a farinha, o leite, o óleo e os ovos
2. Bata até ficar uma massa homogênea, mais para aguada
3. Se for preciso, acrescente um pouco mais de leite para ela ficar igual a um creme
4. Acrescente o fermento e misture com um garfo
5. Reserve

**Recheio**

1. Cozinhe o peito de frango junto com o caldo de galinha
2. Desfie bem o frango e reserve 1 copo da água onde o frango foi cozido
3. Leve à uma panela, com um fio de óleo, a cebola cortada em pedaços pequenos, o sal, a pimenta e o alho poró
4. Faça um refogado, assim que os ingredientes começarem a fritar misture o frango, o milho e o copo com o caldo de cozimento do frango
5. Por último, acrescente o tomate bem picado em cubinhos

**Montagem**

1. Unte uma forma de tamanho médio para grande com manteiga e farinha de trigo
2. Despeje metade da massa
3. Coloque todo o recheio
4. Cubra com uma camada de requeijão e outra camada de queijo mussarela por cima (igual à uma lasanha)
5. Despeje o restante da massa e jogue o queijo ralado por cima de toda massa
6. Leve a forno em temperatura média por aproximadamente 30 minutos, ou até dourar

## :corn: Bolo de milho cremoso na marmita ##

::: warning ATENÇÃO
Apesar de se chamar **Bolo**, a consistência é quase de um pudim... Ou de pamonha mesmo.
:::

### Ingredientes da massa ###

- 2 latas de milho em conserva (400g drenado)
- 4 ovos
- 1 e ½ xícara chá de açúcar (270g)
- 2 colheres (sopa) de manteiga sem sal (40g)
- 200ml de leite de coco (1 vidro)
- 2 colheres sopa de farinha de trigo (16g)
- 1 colher sopa de fermento em pó (12g)


### Ingredientes da cobertura ###

- 1 lata de leite condensado
- 1 pacote de coco ralado em flocos (100g)
- ½ caixa de creme de leite (100g) 

### Modo de preparo ###

1. No liquidificador adicione os ovos, o milho em conserva sem a água, a manteiga sem sal , o leite de coco, a farinha de trigo.
2. Bata bem até que fique homogêneo.
3. Por último adicione o fermento químico em pó e misture delicadamente.
4. Despeje a massa em marmitinhas no tamanho de 9 cm x 13 cm até quase o topo .
5. Leve ao forno preaquecido a 180°C por cerca de 35 – 45 minutos ou até dourar.
6. Retire do forno e deixe esfriar.

### Modo do preparo da cobertura ###

1. Em uma tigela adicione o leite condensado, o coco ralado em flocos e o creme de leite.
2. Misture bem e espalhe sobre o bolo já frio.

## :strawberry: Bolo de morango (Pão de ló) ##

### Ingredientes da massa ###

- 4 ovos
- 2 xícaras de farinha de trigo
- 2 xícaras de açúcar
- 1 xícara de leite (quente)
- 1 colher sopa de  fermento em pó

### Ingredientes da cobertura ###
- 1 creme de leite  
- morangos

### Modo de preparo ###

1. Bater as claras em neve e misturar as gemas. Bater bem.
2. Em seguida, acrescente o açúcar. Bater bem e colocar a farinha aos poucos. 
3. Em seguida misturar o fermento em pó. 
4. Por último o leite fervendo.
5. Misturar bem e colocar numa assadeira untada e assar em forno médio.

### Cobertura ###
1. Bater o creme de leite fresco para o chantilly.
2. Amassar os morangos com açúcar e colocar sobre o bolo.

### Montagem ###

Bolo - 1.a camada de chantilly. Por cima os morangos amassados e por cima outro bolo.
Cobrir com chantilly e enfeitar com morangos.

----

## :custard: Brigadeirão de Microondas ##

### **Ingredientes** ###

- 1 lata de leite condensado
- 1 caixa de creme de leite
- 1 xícara (chá) de chocolate em pó
- 1/4 xícara (chá) de açúcar (NÃO PRECISA)
- 1 colher (sopa) de margarina
- 3 unidades de ovo

### **Modo de preparo** ###

1. Bata os ingredientes no liquidificador e coloque numa fôrma de bolo untada só com margarina.
2. Levar ao microondas em potência alta por 7 minutos aproximadamente (observar até que ele não esteja mole).
3. Desenforme morno.
4. Decore com chocolate granulado.

----

##  :croissant: Muffin Salgado Fácil ##

::: tip DICA
Substitua o presunto se quiser por algo que você goste mais. Ex: Bacon frito em cubinhos, mortadela, peito de peru, salsichão e etc....
 :::

### Ingredientes ###

- 01 xícara (chá) de farinha de trigo com fermento
- 04 colheres (sopa) de maionese
- ¾ xícara (chá) de leite
- 6 fatias de presunto picado
- Salsinha picada a gosto
- 1 pitada de sal

### Modo de preparo ###

1. Pré-aqueça o forno a 180°.
2. Em um recipiente coloque a farinha de trigo com fermento, a maionese, o leite, o presunto picado e a salsinha.
3. Misture bem todos os ingredientes.
4. Tempere com o sal e mexa mais um pouco.
5. Encha as forminhas de muffins até 2/3 da sua capacidade.
6. Leve para assar por 15 a 20 minutos até que enfiando um palito ele saia limpo. 
7. Aproveite!
